--[[
RSoft LED Marquee Generator

A simple luacontroller to generate and manage LED marquees. Always locked, so you can keep the touchscreen in place without worrying that someone might come along and change it.

Touchscreen channel:
	controls

Default marquee channel:
	marquee
]]



--TSLib
--A product of Advanced Mesecons Devices, a Cheapie Systems company
--This is free and unencumbered software released into the public domain.
--See http://unlicense.org/ for more information

--Since LuaCs can't dynamically link libraries,
--you will have to "statically link" it by pasting this file
--above the code that will use it.

local tslib = {
	getColorEscapeSequence = function(color)
		return(string.char(0x1b).."(c@"..color..")")
	end,
	colorize = function(color,message)
		return(string.char(0x1b).."(c@"..color..")"..message..string.char(0x1b).."(c@#FFFFFF)")
	end,
	_mt = {
		setChannel = function(self,channel)
			self._channel = channel
		end,
		getChannel = function(self)
			return(self._channel)
		end,
		draw = function(self)
			digiline_send(self._channel,self._commands)
		end,
		clear = function(self)
			self._commands = {{command="clear"}}
		end,
		setLock = function(self,lock)
			if lock then
				table.insert(self._commands,{command="lock"})
			else
				table.insert(self._commands,{command="unlock"})
			end
		end,
		addLabel = function(self,x,y,label,vertical)
			assert((type(x))=="number","Invalid X position")
			assert((type(y))=="number","Invalid Y position")
			if type(label) ~= "string" then
				label = tostring(label)
			end
			local cmd = {
				command = "addlabel",
				X = x,
				Y = y,
				label = label,
			}
			if vertical then cmd.command = "addvertlabel" end
			table.insert(self._commands,cmd)
		end,
		addImage = function(self,x,y,w,h,tex)
			assert((type(x))=="number","Invalid X position")
			assert((type(y))=="number","Invalid Y position")
			assert((type(w))=="number","Invalid width")
			assert((type(h))=="number","Invalid height")
			if type(tex) ~= "string" then
				tex = tostring(tex)
			end
			local cmd = {
				command = "addimage",
				X = x,
				Y = y,
				W = w,
				H = h,
				texture_name = tex,
			}
			table.insert(self._commands,cmd)
		end,
		addButton = function(self,x,y,w,h,name,label,exit)
			assert((type(x))=="number","Invalid X position")
			assert((type(y))=="number","Invalid Y position")
			assert((type(w))=="number","Invalid width")
			assert((type(h))=="number","Invalid height")
			if type(name) ~= "string" then
				name = tostring(name)
			end
			if type(label) ~= "string" then
				label = tostring(label)
			end
			local cmd = {
				command = "addbutton",
				X = x,
				Y = y,
				W = w,
				H = h,
				name = name,
				label = label,
			}
			if exit then cmd.command = "addbutton_exit" end
			table.insert(self._commands,cmd)
		end,
		addImageButton = function(self,x,y,w,h,name,label,tex,exit)
			assert((type(x))=="number","Invalid X position")
			assert((type(y))=="number","Invalid Y position")
			assert((type(w))=="number","Invalid width")
			assert((type(h))=="number","Invalid height")
			if type(name) ~= "string" then
				name = tostring(name)
			end
			if type(label) ~= "string" then
				label = tostring(label)
			end
			if type(tex) ~= "string" then
				tex = tostring(tex)
			end
			local cmd = {
				command = "addimage_button",
				X = x,
				Y = y,
				W = w,
				H = h,
				name = name,
				label = label,
				image = tex,
			}
			if exit then cmd.command = "addimage_button_exit" end
			table.insert(self._commands,cmd)
		end,
		addField = function(self,x,y,w,h,name,label,default,password)
			assert((type(x))=="number","Invalid X position")
			assert((type(y))=="number","Invalid Y position")
			assert((type(w))=="number","Invalid width")
			assert((type(h))=="number","Invalid height")
			if type(name) ~= "string" then
				name = tostring(name)
			end
			if type(label) ~= "string" then
				label = tostring(label)
			end
			if type(default) ~= "string" then
				default = tostring(default)
			end
			local cmd = {
				command = "addfield",
				X = x,
				Y = y,
				W = w,
				H = h,
				name = name,
				label = label,
				default = default,
			}
			if password then cmd.command = "addpwdfield" end
			table.insert(self._commands,cmd)
		end,
		addTextArea = function(self,x,y,w,h,name,label,default)
			assert((type(x))=="number","Invalid X position")
			assert((type(y))=="number","Invalid Y position")
			assert((type(w))=="number","Invalid width")
			assert((type(h))=="number","Invalid height")
			if type(name) ~= "string" then
				name = tostring(name)
			end
			if type(label) ~= "string" then
				label = tostring(label)
			end
			if type(default) ~= "string" then
				default = tostring(default)
			end
			local cmd = {
				command = "addtextarea",
				X = x,
				Y = y,
				W = w,
				H = h,
				name = name,
				label = label,
				default = default,
			}
			table.insert(self._commands,cmd)
		end,
		addDropdown = function(self,x,y,w,h,name,choices,selected)
			assert((type(x))=="number","Invalid X position")
			assert((type(y))=="number","Invalid Y position")
			assert((type(w))=="number","Invalid width")
			assert((type(h))=="number","Invalid height")
			if not selected then selected = 1 end
			assert((type(selected))=="number","Invalid selection index")
			if type(name) ~= "string" then
				name = tostring(name)
			end
			assert((type(choices) == "table" and #choices >= 1),"Invalid choices list")
			local cmd = {
				command = "adddropdown",
				X = x,
				Y = y,
				W = w,
				H = h,
				name = name,
				choices = choices,
				selected_id = selected,
			}
			table.insert(self._commands,cmd)
		end,
	},
	new = function(self,channel)
		local ret = {}
		for k,v in pairs(self._mt) do
			ret[k] = v
		end
		ret._channel = channel
		ret._commands = {{command="clear"}}
		return ret
	end,
}



-- Initialization
if not mem.started then
	mem.started = true
	-- initialize here
	
	mem.channels = {}
	mem.channels.touchscreen = "controls"
	mem.channels.marquee = "marquee"
	
	mem.text = "Testing!"
	mem.speed = 1
	
	mem.colorcode = "0"
end



-- Detect the wrong channel automatically
if event.type == "digiline" and event.channel and event.channel ~= mem.channels.touchscreen and type(event.msg) == "table" then
	if event.msg.setChannel or not mem.channels.touchscreen then
		mem.channels.touchscreen = event.channel
	end
end

if not mem.channels.touchscreen then return end

screen = tslib:new(mem.channels.touchscreen)



-- Functions
-- set the channel *and* update the screen
function setChannel(channelType, channel)
	mem.channels[channelType] = channel
	
	if channelType == "touchscreen" then
		screen:setChannel(channel)
	end
end

-- add a hook for a certain message
function hook(name, func)
	if event.type and event.type == "digiline" and event.channel == mem.channels.touchscreen and event.msg[name] then
		return func(event.msg)
	end
end

hook("save", function (msg)
	if msg.marqueeChannel then
		mem.channels.marquee = msg.marqueeChannel
	end
	if msg.marqueeColor then
		mem.colorcode = msg.marqueeColor
	end
	if msg.marqueeText then
		mem.text = msg.marqueeText
		digiline_send(mem.channels.marquee, (mem.colorcode and "/" .. mem.colorcode:sub(1, 1) or "") .. mem.text)
	end
	if msg.marqueeSpeed then
		mem.speed = msg.marqueeSpeed
		
		if tonumber(mem.speed) and tonumber(mem.speed) >= 0.2 and tonumber(mem.speed) <= 5 then
			digiline_send(mem.channels.marquee, "start_scroll")
			digiline_send(mem.channels.marquee, "scroll_speed" .. mem.speed)
		else
			digiline_send(mem.channels.marquee, "stop_scroll")
		end
	end
end)

screen:addField(1, 1, 5, 1, "marqueeText", "text to display", mem.text)
screen:addField(1, 2, 5, 1, "marqueeChannel", "marquee channel", mem.channels.marquee)
screen:addField(1, 3, 3, 1, "marqueeSpeed", "marquee delay", mem.speed)
screen:addField(1, 4, 3, 1, "marqueeColor", "color code (0-9, a-r)", mem.colorcode)
screen:addButton(1, 5, 2, 1, "save", "save")

screen:setLock(not mem.demo)

-- tell screen to draw
screen:draw()