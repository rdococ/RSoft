-- Beginning of TSFramework 3
screen = function (data)
	local channel, interact, draw, max, delay = data.channel, data.interact, data.draw, data.max or 15, data.delay or 0.3
	
	if event.type == "interrupt" and
		type(event.iid) == "table" and
		event.iid.type == "TSFramework 3" and
		channel == event.iid.channel and
		mem["TSF3_pending_" .. event.iid.channel] then
		
		local old_elements = _elements
		_elements = mem["TSF3_pending_" .. event.iid.channel]
		
		mem["TSF3_pending_" .. event.iid.channel] = nil
	else
		local old_elements = _elements
		_elements = {{command = "clear"}}
		
		if event.channel == channel then interact(event.msg) end
		draw()
	end
	
	if #_elements > max then
		local pending = {}
		
		for i = max + 1, #_elements do
			table.insert(pending, _elements[max + 1])
			table.remove(_elements, max + 1)
		end
		
		mem["TSF3_pending_" .. channel] = pending
		interrupt(delay, {type = "TSFramework 3", channel = channel})
	end
	
	digiline_send(channel, _elements)
	_elements = old_elements
end

label = function (data)
	table.insert(_elements, {
		command = data.vertical and "addvertlabel" or "addlabel",
		
		X = 0.2 + ((data.pos and (data.pos.x or data.pos[1])) or data.x),
		Y = 0.2 + ((data.pos and (data.pos.y or data.pos[2])) or data.y),
		
		label = tostring(data.label or ""),
	})
end
image = function (data)
	table.insert(_elements, {
		command = "addimage",
		
		X = (data.pos and (data.pos.x or data.pos[1])) or data.x,
		Y = (data.pos and (data.pos.y or data.pos[2])) or data.y,
		
		W = 1.15 * ((data.size and (data.size.w or data.size.x or data.size[1])) or data.w),
		H = 1.15 * ((data.size and (data.size.h or data.size.y or data.size[2])) or data.h),
		
		texture_name = tostring(data.texture or ""),
	})
end
button = function (data)
	table.insert(_elements, {
		command = data.texture and "addimage_button" or "addbutton",
		
		X = (data.pos and (data.pos.x or data.pos[1])) or data.x,
		Y = (data.pos and (data.pos.y or data.pos[2])) or data.y,
		
		W = (data.size and (data.size.w or data.size.x or data.size[1])) or data.w,
		H = (data.size and (data.size.h or data.size.y or data.size[2])) or data.h,
		
		name = data.id,
		label = tostring(data.label or ""),
		texture_name = tostring(data.texture or ""),
	})
end
field = function (data)
	table.insert(_elements, {
		command = data.password and "addpwdfield" or (data.multiline and "addtextarea" or "addfield"),
		
		X = 0.27 + ((data.pos and (data.pos.x or data.pos[1])) or data.x),
		Y = (data.multiline and 0 or 0.3) + ((data.pos and (data.pos.y or data.pos[2])) or data.y),
		
		W = (data.size and (data.size.w or data.size.x or data.size[1])) or data.w,
		H = (data.multiline and 1.17 or 1) * ((data.size and (data.size.h or data.size.y or data.size[2])) or data.h),
		
		name = data.id,
		label = tostring(data.label or ""),
		default = tostring(data.default or "")
	})
end
dropdown = function (data)
	table.insert(_elements, {
		command = "adddropdown",
		
		X = (data.pos and (data.pos.x or data.pos[1])) or data.x,
		Y = 0.07 + ((data.pos and (data.pos.y or data.pos[2])) or data.y),
		
		W = 1.05 * ((data.size and (data.size.w or data.size.x or data.size[1])) or data.w),
		H = (data.size and (data.size.h or data.size.y or data.size[2])) or data.h,
		
		name = data.id,
		label = tostring(data.label or ""),
		selected_id = data.default,
		
		choices = data.choices
	})
end
-- End of TSFramework 3