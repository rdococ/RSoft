--[[
MineX
Version: 1 in-development

Description:
A complete rewrite of MiNESTEP. Code has been rewritten to take advantage of TSFramework, which means that I don't have to store the data from each element individually.
]]

local goto_page = function (p)
	return function (self, screen, msg)
		screen:set_page(p)
	end
end

local exit_button = button:new({
	pos = {7, 1},
	size = {1, 1},
	label = "X",
	on_click = goto_page("desktop")
})

local function update_formula(screen, index)
	local operation = screen:get_content("formula" .. tostring(index) .. "_operation")
	
	local operand1, operand2 = 
		screen:get_content("formula" .. tostring(index) .. "_operand1"),
		screen:get_content("formula" .. tostring(index) .. "_operand2")
	
	if operand1 == mem.calculator["operand1_" .. tostring(index)] and
		operation == mem.calculator["operation_" .. tostring(index)] and
		operand2 == mem.calculator["operand2_" .. tostring(index)] then
			return
	end
	
	if operand1 and operand1:sub(1, 1) == "f" then
		operand1 = update_formula(screen, tonumber(operand1:sub(2, -1)))
	else
		operand1 = tonumber(operand1)
	end
	if operand2 and operand2:sub(1, 1) == "f" then
		operand2 = update_formula(screen, tonumber(operand2:sub(2, -1)))
	else
		operand2 = tonumber(operand2)
	end
	
	local result = 0
	if operand1 and operand2 then
		if operation == "+" or operation == 1 then
			result = operand1 + operand2
		elseif operation == "-" or operation == 2 then
			result = operand1 - operand2
		elseif operation == "*" or operation == 3 then
			result = operand1 * operand2
		elseif operation == "/" or operation == 4 then
			result = operand1 / operand2
		end
	end
	
	mem.calculator["result_" .. tostring(index)] = result
	return result
end

local monitor = screen:new({
	name = "monitor",
	channel = "computer",
	initial_page = "desktop",
	pages = {
		desktop = {
			texteditor = button:new({
				pos = {1, 1},
				size = {2, 1},
				label = "text editor",
				on_click = goto_page("texteditor")
			}),
			calculator = button:new({
				pos = {1, 2},
				size = {2, 1},
				label = "calculator",
				on_click = goto_page("calculator")
			})
		},
		texteditor = {
			filename = field:new({
				pos = {1, 1},
				size = {4, 1}
			}),
			content = field:new({
				pos = {1, 2},
				size = {5, 5},
				multiline = true
			}),
			save = button:new({
				pos = {5, 1},
				size = {1, 1},
				label = "save",
				on_click = function (self, screen, msg)
					if not mem.documents then mem.documents = {} end
					
					if not mem.documents[msg.filename] then
						-- Creating a new document, add it to the document list.
						if not mem.doclist then mem.doclist = {} end
						table.insert(mem.doclist, msg.filename)
					end
					
					mem.documents[msg.filename] = msg.content
				end
			}),
			open = button:new({
				pos = {6, 1},
				size = {1, 1},
				label = "open",
				on_click = function (self, screen, msg)
					if not mem.documents then mem.documents = {} end
					screen:set_content("content", mem.documents[msg.filename] or "file does not exist")
				end
			}),
			exit = exit_button
		},
		calculator = function (screen)
			--[[
			This calculator will be unlike most calculators.
			]]
			
			if not mem.calculator then mem.calculator = {} end
			
			-- TODO: Incremental display system will let us handle more
			-- formulae at once.
			-- Alternatively, just give up on this kind of calculator.
			if not mem.calculator.numFormulae then mem.calculator.numFormulae = 1 end
			
			local page
			page = {
				exit = exit_button,
				update = button:new({
					pos = {6, 7},
					size = {2, 1},
					label = "update"
				}),
				warning_label = label:new({
					pos = {1, mem.calculator.numFormulae + 1},
					label = "(You might be limited by server performance & settings)"
				}),
				add_formula = button:new({
					pos = {1, mem.calculator.numFormulae + 2},
					size = {2, 1},
					label = "add formula",
					on_click = function (self, screen, msg)
						mem.calculator.numFormulae = mem.calculator.numFormulae + 1
					end
				}),
				del_formula = button:new({
					pos = {3, mem.calculator.numFormulae + 2},
					size = {2, 1},
					label = "del formula",
					on_click = function (self, screen, msg)
						local oldNum = mem.calculator.numFormulae
						
						mem.calculator["result_" .. tostring(oldNum)] = nil
						screen:set_content("formula" .. tostring(oldNum) .. "_operand1", nil)
						screen:set_content("formula" .. tostring(oldNum) .. "_operation", nil)
						screen:set_content("formula" .. tostring(oldNum) .. "_operand2", nil)
						
						mem.calculator.numFormulae = mem.calculator.numFormulae - 1
					end
				})
			}
			
			for i = 1, mem.calculator.numFormulae do
				update_formula(screen, i)
				page["formula" .. tostring(i) .. "_operand1"] = field:new({
					pos = {1, i},
					size = {1, 1}
				})
				page["formula" .. tostring(i) .. "_operation"] = field:new({
					pos = {2, i},
					size = {1, 1}
				})
				page["formula" .. tostring(i) .. "_operand2"] = field:new({
					pos = {3, i},
					size = {1, 1}
				})
				page["formula" .. tostring(i) .. "_result"] = label:new({
					pos = {4, i},
					label = "= " .. tostring(mem.calculator["result_" .. tostring(i)])
				})
			end
			
			return page
		end
	}
})

monitor:run()