--[[
TSFramework

By rdococ.
This project has nothing much to do with TSLib other than
providing a more complex version of similar functionality.

TSLib was created by cheapie. They're pretty cool, and TSLib was the
inspiration for this project.


How it works:
Each screen has a table of pages, and each page has a table of elements.
When the luacontroller is activated, the contents of elements such as the
field and dropdown are saved first. Then, hooks and callbacks that you can
program into the elements are run, which may change the current page. Finally,
the new page's elements are drawn.

TODO List in order of priority:
- The ability to add arrays of elements which can be added to and
removed from.
- An incremental display system. This would vastly expand the potential
complexity of screens created with this framework regardless of server
restrictions.


Licensing information:
I'm planning to release this under the MIT License.

Unfortunately, LuaCs cannot dynamically link libraries.
You can complain to Jeija about that (kidding), but in the meantime
you should 'statically link' it by pasting the contents of this file
above the code that will use it.
]]

-- Generic 'new' function that applies a pseudo-metatable and data to a new table.
local generic_new = function (self, data)
	local ret = {}
	
	for k,v in pairs(self._mt) do
		ret[k] = v
	end
	for k,v in pairs(data) do
		ret[k] = v
	end
	
	return ret
end

screen = {
	_mt = {
		get_elements = function (self)
			-- Gets the current table of elements.
			-- Sometimes, a page may be dynamic - a function that returns a new table of elements each time it's called.
			-- If it is a function, we should cache the result of that function to avoid calling it multiple times
			-- in one step.
			
			local page = self.pages[self:get_page()]
			local elements = (self.elements_cache_name == self:get_page()) and self.elements_cache or (type(page) == "table" and page.elements or page)
			
			if type(elements) == "function" then
				elements = elements(self)
			end
			self.elements_cache = elements
			self.elements_cache_name = self:get_page()
			
			return self.elements_cache
		end,
		save_element_content = function (self)
			-- Save the content of the elements currently on the screen.
			
			if event.type ~= "digiline" then return end
			if event.channel ~= self.channel then return end
			
			local page = self.pages[self:get_page()]
			local elements = self:get_elements()
			
			for k, e in pairs(elements) do
				if not e.id then e.id = k end
				if e._content and e.save then
					local content = e:_content(event.msg)
					if content then
						self:set_content(e.id, content)
					end
				elseif e.save and event.msg[e.id] then
					self:set_content(e.id, event.msg[e.id])
				end
			end
		end,
		run_hooks = function (self)
			-- Iterate through the elements on the screen and run their hook functions.
			
			if event.type ~= "digiline" then return end
			if event.channel ~= self.channel then return end
			
			local page = self.pages[self:get_page()]
			local elements = self:get_elements()
			
			for k, e in pairs(elements) do
				if not e.id then e.id = k end
				if e._hook then e:_hook(self, event.msg) end
			end
		end,
		draw = function (self)
			-- Draw the elements to the screen.
			
			local page = self.pages[self:get_page()]
			
			-- Clear the cache - after hooks have been run, the dynamic GUI
			-- might want to change.
			
			-- (I might add an option to turn cache clearing off, as a per-page
			-- or per-screen setting, because you can manipulate the elements
			-- in those hooks instead).
			
			self.elements_cache = nil
			self.elements_cache_name = nil
			local elements = self:get_elements()
			
			local disp = {{command = "clear"}}
			
			for k, e in pairs(elements) do
				if not e.id then e.id = k end
				local cmd = e:_display(self:get_content(e.id))
				if cmd then table.insert(disp, cmd) end
			end
			
			digiline_send(self.channel, disp)
		end,
		run = function (self)
			-- Perform all three actions in the correct order.
			-- Hooks can change the current page, so it's important
			-- to save the content first (so it doesn't get lost),
			-- and draw afterwards (so the new page is drawn).
			
			self:save_element_content()
			self:run_hooks()
			self:draw()
		end,
		
		get_page = function (self)
			-- Get the current page name.
			
			local escape = string.char(27)
			
			if not mem.TSFramework then mem.TSFramework = {} end
			return self.name and mem.TSFramework["page" .. escape .. self.name] or self.initial_page or "initial"
		end,
		set_page = function (self, new_page)
			-- Change to a new page.
			
			local escape = string.char(27)
			
			if not self.name then return end
			if not mem.TSFramework then mem.TSFramework = {} end
			mem.TSFramework["page" .. escape .. self.name] = new_page
		end,
		
		get_content = function (self, page, id)
			-- Get the content of the element within the specified page with a specified id.
			
			if not id then page, id = self:get_page(), page end
			local escape = string.char(27)
			
			if not mem.TSFramework then mem.TSFramework = {} end
			return self.name and id and mem.TSFramework["content" .. escape .. self.name .. escape .. page .. escape .. id]
		end,
		set_content = function (self, page, id, content)
			-- Set the content of the element within the specified page with the specified id.
			
			if not content then page, id, content = self:get_page(), page, id end
			local escape = string.char(27)
			
			if not self.name or not id then return end
			if not mem.TSFramework then mem.TSFramework = {} end
			mem.TSFramework["content" .. escape .. self.name .. escape .. page .. escape .. id] = content
		end
	},
	new = generic_new
}

label = {
	_mt = {
		_display = function (self, content)
			if self.invisible then return end
			return {
				command = self.vertical and "addvertlabel" or "addlabel",
				X = self.pos.x or self.pos[1],
				Y = self.pos.y or self.pos[2],
				label = tostring(self.label or ""),
			}
		end
	},
	new = generic_new
}

image = {
	_mt = {
		_display = function (self, content)
			if self.invisible then return end
			return {
				command = "addimage",
				X = self.pos.x or self.pos[1],
				Y = self.pos.y or self.pos[2],
				W = self.size.w or self.size[1],
				H = self.size.h or self.size[2],
				texture_name = tostring(self.texture or ""),
			}
		end
	},
	new = generic_new
}

button = {
	_mt = {
		_display = function (self, content)
			local cmd = {
				command = self.texture and "addimage_button" or "addbutton",
				X = self.pos.x or self.pos[1],
				Y = self.pos.y or self.pos[2],
				W = self.size.w or self.size[1],
				H = self.size.h or self.size[2],
				name = self.id,
				label = tostring(self.label or ""),
				texture_name = tostring(self.texture or "")
			}
			if self.exit then cmd.command = cmd.command .. "_exit" end
			if self.invisible then return end
			return cmd
		end,
		_hook = function (self, screen, msg)
			if msg[self.id] and self.on_click then
				self:on_click(screen, msg)
			end
		end,
		register_on_click = function (self, f)
			local old_on_click = self.on_click or function () end
			self.on_click = function (...)
				old_on_click(...)
				f(...)
			end
		end
	},
	new = generic_new
}

field = {
	_mt = {
		_display = function (self, content)
			if self.invisible then return end
			content = content or self.default or ""
			
			return {
				command = self.password and "addpwdfield" or (self.multiline and "addtextarea" or "addfield"),
				X = self.pos.x or self.pos[1],
				Y = self.pos.y or self.pos[2],
				W = self.size.w or self.size[1],
				H = self.size.h or self.size[2],
				name = self.id,
				label = tostring(self.label or ""),
				default = tostring(content or "")
			}
		end,
		_hook = function (self, screen, msg)
			if msg[self.id] and self.on_enter then
				self:on_enter(screen, msg)
			end
		end,
		register_on_enter = function (self, f)
			local old_on_enter = self.on_enter or function () end
			self.on_enter = function (...)
				old_on_enter(...)
				f(...)
			end
		end,
		
		save = true
	},
	new = generic_new
}

dropdown = {
	_mt = {
		_display = function (self, content)
			if self.invisible then return end
			local selected = content or self.default or 1
			
			return {
				command = "adddropdown",
				X = self.pos.x or self.pos[1],
				Y = self.pos.y or self.pos[2],
				W = self.size.w or self.size[1],
				H = self.size.h or self.size[2],
				name = self.id,
				choices = self.choices,
				selected_id = selected
			}
		end,
		_content = function (self, msg)
			-- msg[self.id] is the name of a choice.
			-- Find its corresponding index and save that.
			
			if msg[self.id] then
				local choices = self.choices
					
				local seciohc = {}
				for k, v in ipairs(choices) do
					seciohc[v] = k
				end
				
				return seciohc[msg[self.id]] or 1
			end
		end,
		_hook = function (self, screen, msg)
			if msg[self.id] and self.on_select then
				self:on_select(screen, msg)
			end
		end,
		register_on_select = function (self, f)
			local old_on_select = self.on_select or function () end
			self.on_select = function (...)
				old_on_select(...)
				f(...)
			end
		end,
		
		save = true,
	},
	new = generic_new
}