--[[
TS Framework 2

The (not) long-awaited sequel to TS Framework.
Sort-of in development.
]]

-- Generic 'new' function that applies a pseudo-metatable and data to a new table.
-- Pseudo-metatables are necessary because this Lua environment doesn't have real metatables.
local generic_new = function (self, data)
	local ret = {}
	
	for k,v in pairs(self._mt) do
		ret[k] = v
	end
	for k,v in pairs(data) do
		ret[k] = v
	end
	
	return ret
end

screen = {
	_mt = {
		get_current_page_name = function (screen)
			-- I should get the name of the current page.
			
			return mem["TSFramework2:" .. screen.id .. ":pagename"] or screen.initial_page
		end,
		set_current_page_name = function (screen, name)
			-- I should set the current page to the one with the given name.
			
			mem["TSFramework2:" .. screen.id .. ":pagename"] = name
		end,
		
		get_current_page = function (screen)
			-- I should get the current page object.
			
			return screen.pages[screen:get_current_page_name()]
		end,
		
		save_element_content = function (screen)
			-- I should save the content of my elements.
			
			if event.type ~= "digiline" or event.channel ~= screen.channel then return end
			
			local content = mem["TSFramework2:" .. screen.id .. ":page_content:" .. screen:get_current_page_name()]
			if not content then
				content = {}
				mem["TSFramework2:" .. screen.id .. ":page_content:" .. screen:get_current_page_name()] = content
			end
			
			for field, value in pairs(event.msg) do
				content[field] = value
			end
		end,
		
		get_content_for_page_named = function (screen, page_name)
			-- I should return the content of all elements on the given page.
			
			local content = mem["TSFramework2:" .. screen.id .. ":page_content:" .. page_name]
			if not content then
				content = {}
				mem["TSFramework2:" .. screen.id .. ":page_content:" .. page_name] = content
			end
				
			return content
		end,
		get_content_for_current_page = function (screen)
			-- I should return the content of all elements on the current page.
			
			return screen:get_content_for_page_named(screen:get_current_page_name())
		end,
		get_content_of_element = function (screen, element_name)
			-- I should return the content of this element on the current page.
			
			return (screen:get_content_for_current_page())[element_name]
		end,
		
		redraw = function (screen, dont_send)
			-- I should construct a list of commands to send to my touchscreen, and send it.
			
			local commands = {{command = "clear"}}
			local page = screen:get_current_page()
			
			for element_name, element in pairs(page.elements) do
				if element.transient then
					table.insert(commands, element:as_command(element_name))
				else
					table.insert(commands, element:as_command(element_name, screen:get_content_of_element(element_name)))
				end
			end
			
			if not dont_send then digiline_send(screen.channel, commands) end
			return commands
		end,
		
		step = function (screen)
			-- The luacontroller has executed once. I should run my usual processes.
			
			-- Before we do anything, we should check that I'm actually being interacted with.
			if event.type ~= "digiline" or event.channel ~= screen.channel then return end
			if screen.stepped then return end
			
			-- Now we can begin. First, I should save the content of my elements.
			screen:save_element_content()
			
			local page = screen:get_current_page()
			
			-- Then, I should run the current page's callback.
			-- This also does the job of adding dynamic elements that might disappear.
			-- These will also be saved if interacted with.
			
			-- page.on_interact = function (screen)
			-- element.on_interact = function (element_name, element_value, screen)
			if page.on_interact then page.on_interact(screen) end
			
			for element_name, value in pairs(event.msg) do
				if page.elements[element_name] and page.elements[element_name].on_interact then
					page.elements[element_name].on_interact(element_name, value, screen)
				end
			end
			
			-- After this, I should run the current page's "add_dynamic_elements" function.
			-- This allows the page to add dynamic elements that can change or disappear.
			
			-- page.add_dynamic_elements = function (screen)
			page = screen:get_current_page()
			if page.add_dynamic_elements then
				local new_elements = page.add_dynamic_elements(screen)
				if new_elements then
					for element_name, element in pairs(new_elements) do
						page.elements[element_name] = element
						-- TODO: Call on_interact for dynamic elements
						-- This raises a lot of questions.
						-- On_interact callbacks can change what dynamic elements should be drawn.
						-- At the same time, they might want to change properties of a dynamic element.
						-- For now, you're going to have to keep track of those manually.
						-- page.elements[element_name].on_interact(element_name, event.msg[], screen)
					end
				end
			end
			
			-- After that, I can redraw.
			screen:redraw()
			
			screen.stepped = true
		end
	},
	new = generic_new,
	step = function (...)
		-- If the screen object isn't going to be modified or used
		-- in any unusual way, I should just create it and step immediately.
		
		local new_screen = generic_new(...)
		new_screen:step()
		return new_screen
	end
}

page = {
	_mt = {},
	new = generic_new
}

-- The 'label' object supports horizontal and vertical text labels.
label = {
	_mt = {
		as_command = function (self, element_name, element_content)
			return {
				command = self.vertical and "addvertlabel" or "addlabel",
				X = self.pos.x or self.pos[1],
				Y = self.pos.y or self.pos[2],
				label = tostring(self.get_label and self:get_label(element_name, element_content) or self.label or ""),
			}
		end
	},
	new = generic_new
}

-- The 'image' object supports images.
image = {
	_mt = {
		as_command = function (self, element_name, element_content)
			return {
				command = "addimage",
				X = self.pos.x or self.pos[1],
				Y = self.pos.y or self.pos[2],
				W = self.size.w or self.size[1],
				H = self.size.h or self.size[2],
				texture_name = tostring(self.get_texture and self:get_texture(element_name, element_content) or self.texture or ""),
			}
		end
	},
	new = generic_new
}

-- The 'button' object supports text buttons, image buttons and exit buttons.
button = {
	_mt = {
		as_command = function (self, element_name, element_content)
			local cmd = {
				command = (self.get_texture or self.texture) and "addimage_button" or "addbutton",
				X = self.pos.x or self.pos[1],
				Y = self.pos.y or self.pos[2],
				W = self.size.w or self.size[1],
				H = self.size.h or self.size[2],
				name = element_name,
				label = tostring(self.get_label and self:get_label(element_name, element_content) or self.label or ""),
				texture_name = tostring(self.get_texture and self:get_texture(element_name, element_content) or self.texture or "")
			}
			if self.exit then cmd.command = cmd.command .. "_exit" end
			return cmd
		end
	},
	new = generic_new
}

-- The 'field' object supports fields, password fields, and text areas.
field = {
	_mt = {
		as_command = function (self, element_name, element_content)
			return {
				command = self.password and "addpwdfield" or (self.multiline and "addtextarea" or "addfield"),
				X = self.pos.x or self.pos[1],
				Y = self.pos.y or self.pos[2],
				W = self.size.w or self.size[1],
				H = self.size.h or self.size[2],
				name = element_name,
				label = tostring(self.label or ""),
				default = tostring(element_content or self.default or "")
			}
		end
	},
	new = generic_new
}

-- The 'dropdown' object supports dropdowns.
-- You enter a selection number, but the content of the message is the choice itself.
-- This means that saving dropdown choices breaks when there are two choices with the same name.
dropdown = {
	_mt = {
		as_command = function (self, element_name, element_content)
			local selected_id = self.default or 1
			
			if not element_content and type(selected_id) == "string" then
				-- The default ID for the dropdown is a string.
				-- We should then treat it like the name of a choice,
				-- and not the index of a choice.
				element_content = selected_id
			end
			if element_content then
				local choice_to_index = {}
				for k, v in ipairs(self.choices) do
					choice_to_index[v] = choice_to_index[v] or k
				end
				
				selected_id = choice_to_index[element_content]
			end
			
			return {
				command = "adddropdown",
				X = self.pos.x or self.pos[1],
				Y = self.pos.y or self.pos[2],
				W = self.size.w or self.size[1],
				H = self.size.h or self.size[2],
				name = element_name,
				choices = self.choices,
				selected_id = selected_id
			}
		end
	},
	new = generic_new
}