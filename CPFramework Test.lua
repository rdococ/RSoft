panel:new({
	id = "test",
	channel = "test",
	initial_state = "a",
	states = {
		a = state:new({
			output = "State A",
			transitions = {
				left = "b",
				right = "b",
				up = "b",
				down = "b"
			}
		}),
		b = state:new({
			output = "State B",
			transitions = {
				left = "a"
			}
		})
	}
}):step()