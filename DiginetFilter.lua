--[[
Diginet Filter version 1
A RyanTec creation

The Diginet Filter is designed to allow a computer to interface securely with a network of other computers.

Here's how:

1. Connect each computer to a filter.
2. Connect the filters together so that every filter can send and receive messages from every other filter.
3. Protect the network of filters, including any nodes directly adjacent to the network or any filter.
4. Change the names of the filters, so that they are uniquely identifiable. You can do this below.

You can now send messages between computers on the network. Here's how to do it:

1. To send a message, a computer should send a signal to the filter with the 'outgoing' channel name defined below.
   The signal should have a 'receiver' field, set to the name of the filter that the intended recipient is connected to.
   The message being sent should be in a 'message' field.
2. When a computer receives a message, it will get a signal with the 'incoming' channel name defined below.
   The signal will have a 'message' field which contains the content of the message.
   It also contains a 'sender' field which is who sent the message.

Here's some things you can do with it:

1. Create the internet using digilines. When someone sends a message to a web server's filter, the web server can respond
   by sending the formspec elements in its webpage. This is the idea that inspired it!
2. Create an online chatroom using digilines. This one isn't as useful, since you can just use the ingame chat, but it
   might be fun nevertheless!
3. Create an online banking system or cryptocurrency. You can give it purchasing power by selling minegeld for the currency,
   and vice versa.

Here are my plans for it:

1. See if it will garner any interest in VE-Survival.
2. Gather enough resources to start laying down the luacontrollers and wires.
3. Design a new luacontroller OS and a web server to get people interested.
]]

-- These are all configurable.
name = 		"A"						-- The name of this filter. You should definitely change this!
network =	"network"				-- The channel for messages from one filter to the next.
incoming =	"incoming"				-- The channel for messages from the filter to the computer.
outgoing =	"outgoing"				-- The channel for messages from the computer to the filter.
allow_send_to_everyone = false		-- Whether to allow sending a message to every computer at once.

-- Don't touch anything below here unless you know what you're doing!
if
	-- If we've received a message from the computer, pass it to the network of filters.
	event.type == "digiline" and
	event.channel == outgoing and
	type(event.msg) == "table" then
	
	digiline_send(network, {sender = name, receiver = event.msg.receiver, message = event.msg.message})
elseif
	-- If we've received a message from another filter intended for us, pass it to the computer.
	-- The "incoming" message will be sent to the rest of the network, but ignored.
	event.type == "digiline" and
	event.channel == network and
	type(event.msg) == "table" and
	(event.msg.receiver == name or
	allow_send_to_everyone and event.msg.receiver == "everyone") then
	
	digiline_send(incoming, {sender = event.msg.sender, message = event.msg.message})
end