--[[
MiNESTEP X

Operating system for luacontrollers, and sequel to MiNESTEP.
Still in development!

Dependencies:
	TSFramework2

For now, you will have to paste the code from each dependency
before the code itself.

You should be able to find TSFramework2 in the package this came with,
along with other projects.
]]

-- "goto_page" constructs an on_interact function that goes to the page with that name.
function goto_page_named(p)
	return function (_, _, screen)
		return screen:set_current_page_name(p)
	end
end
-- "window" is a constructor for pages which adds an exit button that goes to the desktop.
window = {
	new = function (window, data)
		data.elements.exit = data.elements.exit or button:new({
			pos = {9, 0},
			size = {1, 1},
			label = "X",
			on_interact = goto_page_named("desktop")
		})
		data.elements.title = data.elements.title or label:new({
			pos = {0, 0},
			label = data.title
		})
		
		return page:new(data)
	end
}

-- Now that we have that set up, let's set up the screen.
screen:step({
	id = "computer",
	channel = "computer",
	initial_page = "desktop",
	pages = {
		-- Desktop
		desktop = page:new({
			elements = {
				texteditor = button:new({
					pos = {1, 1},
					size = {2, 1},
					label = "text editor",
					on_interact = goto_page_named("texteditor")
				})
			}
		}),
		
		-- Text editor (multi-page application)
		texteditor = window:new({
			title = "Text Editor",
			elements = {
				filename = field:new({
					pos = {1, 1.5},
					size = {5, 1}
				}),
				filecontent = field:new({
					pos = {1, 2.5},
					size = {8, 5},
					multiline = true
				}),
				open = button:new({
					pos = {6, 1},
					size = {1, 1},
					label = "open",
					on_interact = function (element_name, element_content, screen)
						-- Switch to the page for opening files.
						
						screen:set_current_page_name("texteditor_open")
					end
				}),
				save = button:new({
					pos = {7, 1},
					size = {1, 1},
					label = "save",
					on_interact = function (element_name, element_content, screen)
						-- Switch to the page for opening files.
						
						local save_content = screen:get_content_for_page_named("texteditor_save")
						save_content.filename = screen:get_content_of_element("filename")
						
						screen:set_current_page_name("texteditor_save")
					end
				})
			}
		}),
		texteditor_open = window:new({
			title = "Text Editor - Open File",
			elements = {
				filename = dropdown:new({
					pos = {1, 1.5},
					size = {5, 1},
					choices = mem.document_list
				}),
				open = button:new({
					pos = {6, 1},
					size = {1, 1},
					label = "open",
					on_interact = function (element_name, element_content, screen)
						-- Open the text file with the given filename.
						
						if not mem.documents then mem.documents = {} end
						if not mem.document_list then mem.document_list = {} end
						
						local texteditor_content = screen:get_content_for_page_named("texteditor")
						local open_filename = screen:get_content_for_current_page().filename
						
						texteditor_content.filename = open_filename
						texteditor_content.filecontent = mem.documents[open_filename] or "File does not exist"
						
						-- Go back to the page.
						screen:set_current_page_name("texteditor")
					end
				}),
			}
		}),
		texteditor_save = window:new({
			title = "Text Editor - Save File",
			elements = {
				filename = field:new({
					pos = {1, 1.5},
					size = {5, 1}
				}),
				save = button:new({
					pos = {6, 1},
					size = {1, 1},
					label = "save",
					on_interact = function (element_name, element_content, screen)
						-- Save the text file with the given filename.
						
						if not mem.documents then mem.documents = {} end
						if not mem.document_list then mem.document_list = {} end
						
						local texteditor_content = screen:get_content_for_page_named("texteditor")
						local save_filename = screen:get_content_for_current_page().filename
						
						if not mem.documents[save_filename] then
							table.insert(mem.document_list, save_filename)
						end
						
						texteditor_content.filename = save_filename
						mem.documents[save_filename] = texteditor_content.filecontent
						
						-- Go back to the page.
						screen:set_current_page_name("texteditor")
					end
				}),
			}
		})
	}
})