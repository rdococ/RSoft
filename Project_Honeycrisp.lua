--[[
MiNESTEP

If you are here from an update, simply replace the code of the current luacontroller with this code. Updates are designed to be backwards-compatible with at least the previous version. If there are any problems updating, contact rdococ.

To get this computer up and running:
- Paste this code into a luacontroller.
- Attach a touchscreen to the luacontroller, and set its channel.
- Interact with the touchscreen to send a message to the luacontroller.

Credits:
- piesquared for the codename "Honeycrisp"
- cheapie for the name and TSLib
]]

local ver = "alpha 1.13.3"
local build = 3
local osname = "MiNESTEP"

if event.type == "interrupt" and event.iid == "display" then
	digiline_send(mem.channels.touchscreen, mem.displayQueue[1])
	table.remove(mem.displayQueue, 1)
	
	if #mem.displayQueue > 0 then
		interrupt(0.05, "display")
	end
	
	return
end

--TSLib
--A product of Advanced Mesecons Devices, a Cheapie Systems company
--This is free and unencumbered software released into the public domain.
--See http://unlicense.org/ for more information

--Since LuaCs can't dynamically link libraries,
--you will have to "statically link" it by pasting this file
--above the code that will use it.

local tslib = {
	getColorEscapeSequence = function(color)
		return(string.char(0x1b).."(c@"..color..")")
	end,
	colorize = function(color,message)
		return(string.char(0x1b).."(c@"..color..")"..message..string.char(0x1b).."(c@#FFFFFF)")
	end,
	_mt = {
		setChannel = function(self,channel)
			self._channel = channel
		end,
		getChannel = function(self)
			return(self._channel)
		end,
		draw = function(self)
			digiline_send(self._channel,self._commands)
		end,
		clear = function(self)
			self._commands = {{command="clear"}}
		end,
		setLock = function(self,lock)
			if lock then
				table.insert(self._commands,{command="lock"})
			else
				table.insert(self._commands,{command="unlock"})
			end
		end,
		addLabel = function(self,x,y,label,vertical)
			assert((type(x))=="number","Invalid X position")
			assert((type(y))=="number","Invalid Y position")
			if type(label) ~= "string" then
				label = tostring(label)
			end
			local cmd = {
				command = "addlabel",
				X = x,
				Y = y,
				label = label,
			}
			if vertical then cmd.command = "addvertlabel" end
			table.insert(self._commands,cmd)
		end,
		addImage = function(self,x,y,w,h,tex)
			assert((type(x))=="number","Invalid X position")
			assert((type(y))=="number","Invalid Y position")
			assert((type(w))=="number","Invalid width")
			assert((type(h))=="number","Invalid height")
			if type(tex) ~= "string" then
				tex = tostring(tex)
			end
			local cmd = {
				command = "addimage",
				X = x,
				Y = y,
				W = w,
				H = h,
				texture_name = tex,
			}
			table.insert(self._commands,cmd)
		end,
		addButton = function(self,x,y,w,h,name,label,exit)
			assert((type(x))=="number","Invalid X position")
			assert((type(y))=="number","Invalid Y position")
			assert((type(w))=="number","Invalid width")
			assert((type(h))=="number","Invalid height")
			if type(name) ~= "string" then
				name = tostring(name)
			end
			if type(label) ~= "string" then
				label = tostring(label)
			end
			local cmd = {
				command = "addbutton",
				X = x,
				Y = y,
				W = w,
				H = h,
				name = name,
				label = label,
			}
			if exit then cmd.command = "addbutton_exit" end
			table.insert(self._commands,cmd)
		end,
		addImageButton = function(self,x,y,w,h,name,label,tex,exit)
			assert((type(x))=="number","Invalid X position")
			assert((type(y))=="number","Invalid Y position")
			assert((type(w))=="number","Invalid width")
			assert((type(h))=="number","Invalid height")
			if type(name) ~= "string" then
				name = tostring(name)
			end
			if type(label) ~= "string" then
				label = tostring(label)
			end
			if type(tex) ~= "string" then
				tex = tostring(tex)
			end
			local cmd = {
				command = "addimage_button",
				X = x,
				Y = y,
				W = w,
				H = h,
				name = name,
				label = label,
				image = tex,
			}
			if exit then cmd.command = "addimage_button_exit" end
			table.insert(self._commands,cmd)
		end,
		addField = function(self,x,y,w,h,name,label,default,password)
			assert((type(x))=="number","Invalid X position")
			assert((type(y))=="number","Invalid Y position")
			assert((type(w))=="number","Invalid width")
			assert((type(h))=="number","Invalid height")
			if type(name) ~= "string" then
				name = tostring(name)
			end
			if type(label) ~= "string" then
				label = tostring(label)
			end
			if type(default) ~= "string" then
				default = tostring(default)
			end
			local cmd = {
				command = "addfield",
				X = x,
				Y = y,
				W = w,
				H = h,
				name = name,
				label = label,
				default = default,
			}
			if password then cmd.command = "addpwdfield" end
			table.insert(self._commands,cmd)
		end,
		addTextArea = function(self,x,y,w,h,name,label,default)
			assert((type(x))=="number","Invalid X position")
			assert((type(y))=="number","Invalid Y position")
			assert((type(w))=="number","Invalid width")
			assert((type(h))=="number","Invalid height")
			if type(name) ~= "string" then
				name = tostring(name)
			end
			if type(label) ~= "string" then
				label = tostring(label)
			end
			if type(default) ~= "string" then
				default = tostring(default)
			end
			local cmd = {
				command = "addtextarea",
				X = x,
				Y = y,
				W = w,
				H = h,
				name = name,
				label = label,
				default = default,
			}
			table.insert(self._commands,cmd)
		end,
		addDropdown = function(self,x,y,w,h,name,choices,selected)
			assert((type(x))=="number","Invalid X position")
			assert((type(y))=="number","Invalid Y position")
			assert((type(w))=="number","Invalid width")
			assert((type(h))=="number","Invalid height")
			if not selected then selected = 1 end
			assert((type(selected))=="number","Invalid selection index")
			if type(name) ~= "string" then
				name = tostring(name)
			end
			assert((type(choices) == "table" and #choices >= 1),"Invalid choices list")
			local cmd = {
				command = "adddropdown",
				X = x,
				Y = y,
				W = w,
				H = h,
				name = name,
				choices = choices,
				selected_id = selected,
			}
			table.insert(self._commands,cmd)
		end,
	},
	new = function(self,channel)
		local ret = {}
		for k,v in pairs(self._mt) do
			ret[k] = v
		end
		ret._channel = channel
		ret._commands = {{command="clear"}}
		return ret
	end,
}



-- Initialization
if not mem.started then
	mem.started = true
	-- initialize here
	mem.running = "desktop"
	mem.tasks = 0
	
	mem.channels = {}
	-- mem.channels.touchscreen = "os"
	mem.channels.internet = "internet"
	
	mem.docs = {}
	
	mem.screenWidth = 10
	mem.screenHeight = 8
end

if not mem.screenWidth then
	mem.screenWidth = 10
	mem.screenHeight = 8
end

-- compatibility with builds before 23 Aug 2018
if mem.channel then
	mem.channels = {}
	mem.channels.touchscreen = mem.channel
	mem.channel = nil
end



-- Detect the wrong channel automatically
if event.type == "digiline" and event.channel and event.channel ~= mem.channels.touchscreen and type(event.msg) == "table" then
	if event.msg.setChannel or not mem.channels.touchscreen then
		mem.channels.touchscreen = event.channel
	end
end

if not mem.channels.touchscreen then return end

screen = tslib:new(mem.channels.touchscreen)



-- Functions
-- set the channel *and* update the screen
function setChannel(channelType, channel)
	mem.channels[channelType] = channel
	
	if channelType == "touchscreen" then
		screen:setChannel(channel)
	end
end

-- add a hook for a certain message
function hook(name, func)
	if event.type and event.type == "digiline" and event.channel == mem.channels.touchscreen and event.msg[name] then
		return func(event.msg)
	end
end

-- close an app, and modify mem.tasks
function closeApp(app)
	if mem["ram_" .. app] then
		mem["ram_" .. app] = nil
		mem.tasks = mem.tasks - 1
	end
	
	if mem.running == app then
		switch("desktop")
	end
end

-- switch apps
function switch(app, r)
	-- clear screen so that we don't draw too many things to it
	screen:clear()
	
	-- ensure that the new app doesn't accidentally think that the event was meant for it instead of the old app
	event = {}
	mem.running = app
	
	if not mem["ram_" .. app] then
		mem.tasks = mem.tasks + 1
	end
	
	-- clear the display queue, so incremental GUI elements aren't added to the wrong application
	mem.displayQueue = {}
	
	screen:addLabel(1, 1, "Switching to " .. app .. "...")
	
	if not r then
		interrupt(0.3)
	end
	
	return r and run(app)
end

-- sort document keys consistently for texteditor dropdown
-- sorts semi-asciibetically, might break with unicode
function sortdockeys()
	local dockeys = {""}
	
	for key, _ in pairs(mem.docs) do
		table.insert(dockeys, key)
	end
	
	table.sort(dockeys, function (a, b)
		local a, b = tostring(a):lower(), tostring(b):lower()
		
		while a:sub(1, 1) == b:sub(1, 1) and a:len() > 0 and b:len() > 0 do
			a = a:sub(2, -1)
			b = b:sub(2, -1)
		end
		
		return (string.byte(a) or 0) < (string.byte(b) or 0)
	end)
	
	return dockeys
end

-- display interrupt function. schedules an interrupt that adds GUI elements incrementally if mem.running stays the same
local displayingAfter
function displayAfter(...)
	if not mem.displayQueue then
		mem.displayQueue = {}
	end
	
	for _, f in ipairs({...}) do
		local screen2 = tslib:new(mem.channels.touchscreen)
		screen2._commands = {}
		f(screen2)
		table.insert(mem.displayQueue, screen2._commands)
	end
	
	if not displayingAfter then
		interrupt(0.05, "display")
		displayingAfter = true
	end
end

-- helper function to determine what to do when you get a HTTP response
function hookHTTP(success, failure, timeout)
	if event.type ~= "digiline" or event.channel ~= mem.channels.internet then return end
	
	local result = event.msg
	
	if result.completed then
		if result.succeeded then
			return success(result.data)
		else
			if result.timeout then
				return timeout(result.code, result.data)
			else
				return failure(result.code, result.data)
			end
		end
	end
end



-- Apps
apps = {}
local applist = {"about", "texteditor", "calculator", "settings"}

apps.desktop = function (ram)
	local x, y = 0, 0
	
	for _, app in ipairs(applist) do
		if app ~= "desktop" then
			if hook("launch_" .. app, function (msg)
				switch(app)
				return true
			end) then return end
			hook("exit_" .. app, function (msg)
				closeApp(app)
				return true
			end)
			
			screen:addButton(x, y, 2, 1, "launch_" .. app, app)
			
			if mem["ram_" .. app] then
				screen:addButton(x + 1.75, y, 0.85, 1, "exit_" .. app, "X")
			end
			
			y = y + 1
		end
	end
	
	hook("lockTouchscreen", function (msg)
		if mem.demo then
			screen:addLabel(1.5, 7.6, "Disabled in demo mode.")
		else
			screen:setLock(not mem.locked)
			mem.locked = not mem.locked
		end
	end)
	
	screen:addButton(mem.screenWidth - 1.5, 0, 1.5, 1, "lockTouchscreen", mem.locked and "unlock" or "lock")
end

apps.texteditor = function (ram)
	if not ram.text then ram.text = "" end
	if not ram.filename then ram.filename = "" end
	if not ram.mode then ram.mode = "edit" end
	if not ram.fileselect then ram.fileselect = "" end
	
	if not ram.openindex then ram.openindex = 1 end
	
	if ram.mode == "save" or ram.mode == "open" then
		hook("filename", function (msg)
			ram.filename = msg.filename
		end)
		hook("fileselect", function (msg)
			local sortedkeys = sortdockeys()
			local indexes = {}
			
			for k, i in pairs(sortedkeys) do
				indexes[i] = k
			end
			
			ram.fileselect = msg.fileselect
			ram.openindex = indexes[ram.fileselect]
		end)
		
		hook("doSave", function (msg)
			mem.docs[ram.filename] = ram.text
			ram.mode = "edit"
		end)
		hook("doOpen", function (msg)
			ram.filename = msg.fileselect
			ram.openindex = 1
			
			ram.text = mem.docs[ram.filename] or ""
			ram.mode = "edit"
		end)
		
		hook("back", function (msg)
			ram.mode = "edit"
		end)
	else
		hook("text", function (msg)
			ram.text = msg.text
		end)
		
		hook("gotoSave", function (msg)
			ram.mode = "save"
		end)
		hook("gotoOpen", function (msg)
			ram.mode = "open"
		end)
		hook("doSave", function (msg)
			if not ram.filename or ram.filename == "" then
				ram.mode = "save"
			else
				mem.docs[ram.filename] = ram.text
				ram.mode = "edit"
			end
		end)
	end
	
	if ram.mode == "save" then
		screen:addField(0.6, 0.625, mem.screenWidth - (10 - 5.75), 1, "filename", "", ram.filename)
		screen:addButton(mem.screenWidth - 4, 0.325, 1, 1, "doSave", "save")
		screen:addButton(mem.screenWidth - (10 - 6.85), 0.325, 1, 1, "back", "back")
	elseif ram.mode == "open" then
		local sortedkeys = sortdockeys()
		
		screen:addDropdown(0.4, 0.4, mem.screenWidth - 4, 1, "fileselect", sortedkeys, ram.openindex)
		screen:addButton(mem.screenWidth - 4, 0.325, 1, 1, "doOpen", "open")
		screen:addButton(mem.screenWidth - (10 - 6.85), 0.325, 1, 1, "back", "back")
	else
		screen:addTextArea(0.5, 1, mem.screenWidth - (10 - 9.7), mem.screenHeight - (8 - 6.5), "text", "", ram.text)
		
		screen:addButton(mem.screenWidth - 5, 0, 1.2, 1, "gotoOpen", "open")
		screen:addButton(mem.screenWidth - 4, 0, 1.2, 1, "doSave", "save")
		screen:addButton(mem.screenWidth - 3, 0, 1, 1, "gotoSave", "as")
	end
	
	return ram
end

apps.calculator = function (ram)
	-- set default values
	if not ram[1] then ram[1] = "0" end
	if not ram[2] then ram[2] = "" end
	if not ram.operation then ram.operation = "" end
	if not ram.selected then ram.selected = 1 end
	if not ram.answer then ram.answer = 0 end
	if ram.reenter == nil then ram.reenter = true end
	
	
	
	-- hook number buttons
	for i = 0, 9 do
		hook(tostring(i), function (msg)
			ram[ram.selected] = (ram.reenter and "" or ram[ram.selected]) .. tostring(i)
			ram.reenter = ram.reenter and i == 0 and ram[ram.selected]:sub(1, 1) == "0"
		end)
	end
	
	displayAfter(function (screen)
		for row = 3, 1, -1 do
			for column = 1, 3 do
				local number = ((4-row)*3 - 3) + column
				screen:addButton(column, row + 1, 1, 1, tostring(number), tostring(number))
			end
		end
		screen:addButton(1, 5, 1, 1, "0", "0")
	end)
	
	-- hook decimal point button
	hook(".", function (msg)
		ram[ram.selected] = ((ram.reenter or ram[ram.selected]:len() < 1) and "0" or ram[ram.selected]) .. "."
		ram.reenter = false
	end)
	
	
	
	-- hook operations
	local oplist = {
		"+", "-", "*", "/",
		"^", "%", "ln", "logb",
		"sin", "cos", "tan", "asin",
		"acos", "atan", "atan2", "sqrt"
	}
	local operations = {
		["+"] = function (a, b) return a + b end,
		["-"] = function (a, b) return a - b end,
		["*"] = function (a, b) return a * b end,
		["/"] = function (a, b) return a / b end,
		["^"] = function (a, b) return a ^ b end,
		["%"] = function (a, b) return a % b end,
		logb = function (a, b) return math.log(a) / math.log(b) end,
		root = function (a, b) return b ^ (1 / a) end,
		atan2 = math.atan2 or math.atan
	}
	local functions = {
		ln = math.log,
		sin = math.sin,
		cos = math.cos,
		tan = math.tan,
		asin = math.asin,
		acos = math.acos,
		atan = math.atan,
		sqrt = math.sqrt
	}
	
	for _, op in ipairs(oplist) do
		hook(op, function (msg)
			if operations[op] then
				ram.operation = op
				ram.selected = 2
				ram.reenter = true
			elseif functions[op] then
				ram.answer = functions[op](ram[ram.selected])
				ram[ram.selected] = functions[op](ram[ram.selected])
				ram.reenter = true
			end
		end)
	end
	
	
	
	-- hook constants
	local constlist = {"pi", "e", "phi"}
	local constants = {
		pi = math.pi,
		e = math.exp(1),
		phi = (1 + math.sqrt(5)) / 2
	}
	
	for i, name in pairs(constlist) do
		hook(name, function (msg)
			ram[ram.selected] = tostring(constants[name])
			ram.reenter = true
		end)
		screen:addButton(i + 4, 1, 1, 1, name, name)
	end
	
	
	-- hook variable buttons
	local variables = {"x", "y", "z"}
	for _, vname in ipairs(variables) do
		if not ram[vname] then ram[vname] = 0 end
		hook(vname, function (msg)
			ram[ram.selected] = tostring(ram[vname])
			ram.reenter = true
		end)
		hook(vname .. "=", function (msg)
			if ram[ram.selected] == ram[vname] and ram.reenter then
				ram[vname] = 0
			else
				if ram[1] and ram.operation and ram[2] and ram.operation:len() > 0 then
					local result = operations[ram.operation](tonumber(ram[1]) or 0, tonumber(ram[2]) or 0)
					ram[vname] = result
					ram.answer = result
					ram[1] = tostring(result)
					ram[2] = ""
					ram.operation = ""
					ram.selected = 1
					ram.reenter = true
				else
					ram[vname] = tonumber(ram[ram.selected])
				end
				ram.reenter = true
			end
		end)
	end
	
	displayAfter(function (screen)
		for i, vname in ipairs(variables) do
			screen:addButton((i - 1) * 2 + 1, 6, 1, 1, vname, vname)
			screen:addButton((i - 1) * 2 + 2, 6, 1, 1, vname .. "=", vname .. "=")
		end
	end)
	
	
	
	hook("clearAll", function (msg)
		ram[1] = "0"
		ram[2] = ""
		ram.operation = ""
		ram.selected = 1
		ram.reenter = true
	end)
	hook("clearOne", function (msg)
		ram[ram.selected] = ""
		ram.reenter = true
	end)
	hook("backspace", function (msg)
		ram[ram.selected] = ram[ram.selected]:sub(1, -2)
		ram.reenter = false
	end)
	hook("answer", function (msg)
		ram[ram.selected] = ram.answer
		ram.reenter = true
	end)
	
	hook("=", function (msg)
		local result = operations[ram.operation](tonumber(ram[1]) or 0, tonumber(ram[2]) or 0)
		ram.answer = result
		ram[1] = tostring(result)
		ram[2] = ""
		ram.operation = ""
		ram.selected = 1
		ram.reenter = true
	end)
	
	local add_ops = function (start, last, screen)
		local x, y = 4, 1
		for row, op in ipairs(oplist) do
			if row > last then return end
			if row >= start then
				screen:addButton(x, y + 1, 1, 1, op, op)
			end
			y = y + 1
			if y > 4 then
				x, y = x + 1, 1
			end
		end
	end
	
	for i = 1, #oplist, 10 do
		local j = math.min(i + 9, #oplist)
		displayAfter(function (screen)
			add_ops(i, j, screen)
		end)
	end
	
	screen:addButton(2, 5, 1, 1, ".", ".")
	screen:addButton(3, 5, 1, 1, "=", "=")
	
	screen:addButton(1, 1, 1, 1, "clearAll", "C")
	screen:addButton(2, 1, 1, 1, "clearOne", "CE")
	screen:addButton(3, 1, 1, 1, "backspace", "<x")
	screen:addButton(4, 1, 1, 1, "answer", "ans")
	
	-- add label
	screen:addLabel(0.5, 0.5, ram[1] .. " " .. ram.operation .. " " .. ram[2])
	
	return ram
end

apps.about = function (ram)
	local about = {
		osname .. " " .. ver,
		"",
		"MiNESTEP is an operating system created by RyanTec.",
		"",
		"Programs now have their own memory tables,",
		"    and you can leave a program without",
		"    erasing its memory by minimizing it.",
		"",
		"This system is much easier to program as you don't",
		"    have to worry about GUI element conflicts, and",
		"    it is simply an extension of the original",
		"    singletasking system.",
		"",
		"To get your own copy of this OS, simply copy the",
		"    code from the luacontroller, paste it to your",
		"    own luacontroller, and attach a touchscreen.",
		"    The channel is determined automatically."
	}
	
	for i, content in ipairs(about) do
		screen:addLabel(0, (i - 1) / 3, content)
	end
end

apps.settings = function (ram)
	if mem.demo then
		local about = {
			"Settings are disabled in demo mode.",
			"",
			"To get your own copy of this OS, simply copy",
			"    the code from the luacontroller."
		}
		
		for i, content in ipairs(about) do
			screen:addLabel(0, (i - 1) / 3, content)
		end
		
		return
	elseif not mem.locked and not mem.settingsunlocked then
		local about = {
			"Settings are disabled when the computer is unlocked.",
			"",
			"This is to prevent random people from changing",
			"    settings without authorization.",
			"",
			"Lock the computer and then there will be a setting",
			"    to unlock the settings while the computer is unlocked."
		}
		
		for i, content in ipairs(about) do
			screen:addLabel(0, (i - 1) / 3, content)
		end
		
		return
	end
	
	if ram.checkForUpdates then
		hook("back", function ()
			ram.checkForUpdates = false
			ram.updateState = nil
		end)
	end
	
	hook("checkForUpdates", function (msg)
		ram.checkForUpdates = true
		ram.updateState = nil
	end)
	
	if ram.checkForUpdates then
		-- https://gitlab.com/rdococ/RSoft/raw/master/Project_Honeycrisp.lua
		
		if not ram.updateState then ram.updateState = "checkForLatestVersion" end
		
		if event.type == "digiline" and event.channel == mem.channels.internet then
			if ram.updateState == "checkForLatestVersion" then
				hookHTTP(function (data)
					-- success
					ram.latestVersion = tonumber(data)
					if tonumber(data) == build then
						ram.updateState = "upToDate"
					elseif tonumber(data) < build then
						ram.updateState = "laterVersion"
					elseif tonumber(data) > build then
						ram.updateState = "getCode"
					end
				end, function (code, data)
					ram.updateState = "latestVersionError"
				end, function (code)
					ram.updateState = "latestVersionTimeout"
				end)
			elseif ram.updateState == "getCode" then
				hookHTTP(function (data)
					ram.updateState = "displayCode"
					ram.latestCode = data
				end, function (code, data)
					ram.updateState = "getCodeError"
				end, function (code)
					ram.updateState = "getCodeTimeout"
				end)
			end
		end
		
		if ram.updateState == "checkForLatestVersion" then
			screen:addLabel(0.5, 0.5, "Checking latest version...")
			screen:addLabel(0.5, 1, "(You need the digiline NIC to connect to the internet!)")
			digiline_send(mem.channels.internet, "https://gitlab.com/rdococ/RSoft/raw/master/MiNESTEP_build.txt")
		elseif ram.updateState == "getCode" then
			screen:addLabel(0.5, 0.5, "Getting code for latest version...")
			digiline_send(mem.channels.internet, "https://gitlab.com/rdococ/RSoft/raw/master/Project_Honeycrisp.lua")
		elseif ram.updateState == "displayCode" then
			screen:addLabel(0.5, 0.5, "A new version is available.")
			screen:addTextArea(1, 1, mem.screenWidth - (10 - 8.7), mem.screenHeight - (8 - 5.5), "text", "", ram.latestCode)
		elseif ram.updateState == "upToDate" then
			screen:addLabel(0.5, 0.5, "Your system is up to date.")
		elseif ram.updateState == "latestVersionError" then
			screen:addLabel(0.5, 0.5, "Error checking the latest version!")
		elseif ram.updateState == "latestVersionTimeout" then
			screen:addLabel(0.5, 0.5, "Timeout checking the latest version!")
		elseif ram.updateState == "getCodeError" then
			screen:addLabel(0.5, 0.5, "Error getting the latest code!")
		elseif ram.updateState == "getCodeTimeout" then
			screen:addLabel(0.5, 0.5, "Timeout getting the latest code!")
		elseif ram.updateState == "laterVersion" then
			local text = {
				"Your version is later than the latest version!",
				"One of three things has happened:",
				"- You have changed the build number.",
				"- You got your hands on a development build.",
				"- You've taken a copy from the future!"
			}
			
			for i, content in ipairs(text) do
				screen:addLabel(0.5, (i - 1) / 3 + 0.5, content)
			end
		end
		
		screen:addButton(0.75, mem.screenHeight - 2, 1.5, 1, "back", "back")
		
		return
	end
	
	hook("save", function (msg)
		if msg.touchscreenChannel then
			setChannel("touchscreen", msg.touchscreenChannel)
		end
		if msg.internetChannel then
			setChannel("internet", msg.internetChannel)
		end
		
		if msg.screenWidth then
			mem.screenWidth = msg.screenWidth
		end
		if msg.screenHeight then
			mem.screenHeight = msg.screenHeight
		end
	end)
	
	hook("unlockSettings", function (msg)
		if mem.locked then
			mem.settingsunlocked = not mem.settingsunlocked
		end
	end)
	
	screen:addField(1, 1, 5, 1, "touchscreenChannel", "touchscreen channel", mem.channels.touchscreen)
	screen:addField(1, 2, 5, 1, "internetChannel", "internet channel", mem.channels.internet)
	screen:addField(1, 3, 2, 1, "screenWidth", "screen width", mem.screenWidth)
	screen:addField(3, 3, 2, 1, "screenHeight", "height", mem.screenHeight)
	
	screen:addButton(0.75, mem.locked and mem.screenHeight - 4 or mem.screenHeight - 3, 3.5, 1, "checkForUpdates", "check for updates")
	
	if mem.locked then
		screen:addButton(0.75, mem.screenHeight - 3, 5.5, 1, "unlockSettings", (mem.settingsunlocked and "disable" or "enable") .. " settings while unlocked")
	end
	
	screen:addButton(0.75, mem.screenHeight - 2, 1.5, 1, "save", "save")
end


-- Running
function run(app)
	-- set the running app to this one
	mem.running = app
	
	-- if its ram doesn't exist, create it
	if not mem["ram_" .. app] then
		mem["ram_" .. app] = {}
	end
	
	-- clear screen
	screen:clear()
	
	-- run the app's code with its ram and set the new ram to it
	-- returning ram is only necessary if you reset it to a new table
	-- but I would do it anyway just in case
	mem["ram_" .. app] = apps[app](mem["ram_" .. app]) or mem["ram_" .. app]
	
	-- add exit
	if app ~= "desktop" then
		screen:addButton(mem.screenWidth - (10 - 8.375), 0, 0.85, 1, "minimizeProgram", "_")
		screen:addButton(mem.screenWidth - 1, 0, 0.85, 1, "exitProgram", "X")
	end
	
	-- this should run after the app is so the app can save the stuff in fields
	if app ~= "desktop" then
		-- minimizing switches the application without erasing RAM
		hook("minimizeProgram", function (msg)
			switch("desktop")
		end)
		
		-- exiting switches the application and erases RAM
		hook("exitProgram", function (msg)
			closeApp(app)
		end)
	end
	
	-- add a dummy button to save stuff to fields before exiting the touchscreen
	screen:addButton(0, mem.screenHeight - (8 - 7.4), 1.5, 1, "leaveTouchscreen", "leave", true)
	
	-- add time
	local date = os.datetable()
	
	-- pad minutes
	local date_min = tostring(date.min)
	if date_min:len() < 2 then date_min = "0" .. date_min end
	
	local datestring = date.year .. "-" .. date.month .. "-" .. date.day .. "   " .. date.hour .. ":" .. date_min
	
	screen:addLabel(mem.screenWidth - (10 - 7.5), mem.screenHeight - (8 - 7.6), datestring)
	screen:addLabel(mem.screenWidth - (10 - 5.5), mem.screenHeight - (8 - 7.6), tostring(mem.tasks) .. (mem.tasks == 1 and " task" or " tasks"))
end



-- run the current app
run(mem.running)

-- tell screen to draw
screen:draw()