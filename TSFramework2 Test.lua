--[[
TSFramework2 Test

Used to test basic elements of TSFramework2.
]]

local test = screen:new({
	id = "test",
	channel = "test",
	initial_page = "a",
	pages = {
		a = page:new({
			elements = {
				name = label:new({
					pos = {1, 1},
					label = "page a"
				}),
				ab = button:new({
					pos = {1, 2},
					size = {2, 1},
					label = "go to page b",
					on_interact = function (element_name, element_content, screen)
						screen:set_current_page_name("b")
					end
				}),
				field = field:new({
					pos = {1, 4},
					size = {3, 1},
					label = "test field"
				}),
				dropdown = dropdown:new({
					pos = {1, 6},
					size = {3, 1},
					label = "test dropdown",
					choices = {"a", "b", "c", "c", "d"},
				})
			}
		}),
		b = page:new({
			elements = {
				name = label:new({
					pos = {1, 1},
					label = "page b"
				}),
				ba = button:new({
					pos = {1, 2},
					size = {2, 1},
					label = "go to page a",
					on_interact = function (element_name, element_content, screen)
						screen:set_current_page_name("a")
					end
				}),
				field = field:new({
					pos = {1, 4},
					size = {3, 1},
					label = "test field"
				}),
				dropdown = dropdown:new({
					pos = {1, 6},
					size = {3, 1},
					label = "test dropdown",
					choices = {"a", "b", "c", "c", "d"},
				})
			}
		})
	}
})

test:step()