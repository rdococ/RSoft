--[[
CPFramework

Makes it easier to create programs with the Digilines Control Panels.
]]

-- Generic 'new' function that applies a pseudo-metatable and data to a new table.
-- Pseudo-metatables are necessary because this Lua environment doesn't have real metatables.
local generic_new = function (self, data)
	local ret = {}
	
	for k,v in pairs(self._mt) do
		ret[k] = v
	end
	for k,v in pairs(data) do
		ret[k] = v
	end
	
	return ret
end

panel = {
	_mt = {
		set_current_state_name = function (panel, state_name)
			mem["CPFramework:" .. panel.id] = state_name
		end,
		get_current_state_name = function (panel)
			return mem["CPFramework:" .. panel.id] or panel.initial_state
		end,
		
		get_current_state = function (panel)
			return panel.states[panel:get_current_state_name()]
		end,
		
		step = function (panel)
			if event.type ~= "digiline" then return end
			if event.channel ~= panel.channel then return end
			
			local current_state = panel:get_current_state()
			local new_state_name = current_state.on_interact and current_state:on_interact(event.msg) or
				current_state.transitions and current_state.transitions[event.msg] or panel:get_current_state_name()
			
			panel:set_current_state_name(new_state_name)
			current_state = panel:get_current_state()
			
			digiline_send(panel.channel, current_state.get_output and current_state:get_output() or
				current_state.output)
		end
	},
	new = generic_new
}

state = {
	_mt = {},
	new = generic_new
}