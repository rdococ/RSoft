-- Beginning of Demonstration
local pages = {
	{
		"Welcome to the demonstration for TSFramework 3.",
		"",
		"TSFramework 3 is a piece of luacontroller software that makes it easy to",
		"program your own user interfaces with touchscreens.",
		"",
		"This demonstration is a simple digital book with examples, showing",
		"you how to use TSFramework 3."
	},
	{
		"You will need to copy the code between the comments that are marked:",
		" -- Beginning of TSFramework 3",
		"and",
		" -- End of TSFramework 3",
		"",
		"After that is the code for this demo, which you can look at as an example of",
		"using TSFramework 3."
	},
	{
		"Here is a simple example.",
		"It shows a counter, and a button. It counts how many times you click",
		"the button.",
		"",
		function (y)
			field {
				multiline = true,
				id = "example",
				pos = {0, y},
				size = {10, 7-y},
				default = [[
mem.clicks = mem.clicks or 0

screen {
	channel = "test",
	
	interact = function (msg)
	
		if msg.add then
			mem.clicks = mem.clicks + 1
		end
		
	end,
	
	draw = function ()
	
		label {
			id = "clicks",
			
			pos = {0, 0},
			
			label = tostring(mem.clicks)
		}
		
		button {
			id = "add",
			
			pos = {1, 0},
			size = {1, 1},
			
			label = "+"
		}
		
	end
}
]],
			}
		end
	}
}
mem.page = mem.page or 1

screen {
	channel = "test",
	interact = function (msg)
		if msg.first then
			mem.page = 1
		elseif msg.previous and mem.page > 1 then
			mem.page = mem.page - 1
		elseif msg.next and mem.page < #pages then
			mem.page = mem.page + 1
		elseif msg.last then
			mem.page = #pages
		end
	end,
	draw = function ()
		button {
			id = "first",
			pos = {0, 0},
			size = {1, 1},
			label = "<<"
		}
		
		button {
			id = "previous",
			pos = {1, 0},
			size = {1, 1},
			label = "<"
		}
		
		button {
			id = "next",
			pos = {8, 0},
			size = {1, 1},
			label = ">"
		}
		
		button {
			id = "last",
			pos = {9, 0},
			size = {1, 1},
			label = ">>"
		}
		
		label {
			id = "pagenum",
			pos = {0, 7},
			label = "Page " .. tostring(mem.page) .. "/" .. tostring(#pages)
		}
		
		for num, text in ipairs(pages[mem.page]) do
			if type(text) == "function" then
				text(num / 2 + 0.5)
			else
				label {
					id = "text",
					pos = {0, num / 2 + 0.5},
					label = text
				}
			end
		end
	end
}
-- End of demonstration.