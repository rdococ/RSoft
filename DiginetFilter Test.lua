--[[
Diginet Test Computer
]]

name = "A"
receivers = {}

for i, v in ipairs({"A", "B", "C", "D", "everyone"}) do
	if v ~= name then
		table.insert(receivers, v)
	end
end

if event.type == "interrupt" then
	port.a, port.b, port.c, port.d = false, false, false, false
elseif event.type == "digiline" and event.channel == "incoming" and type(event.msg) == "table" then
	digiline_send("debug", event.msg.sender .. " says " .. event.msg.message)
	
	port.a, port.b, port.c, port.d = true, true, true, true
	interrupt(1)
elseif event.type == "on" then
	local receiver = receivers[math.random(1, #receivers)]
	
	digiline_send("debug", "Sent a message to " .. receiver)
	digiline_send("outgoing", {receiver = receiver, message = "This is a message from " .. name .. "!"})
end